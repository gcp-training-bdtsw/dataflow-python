# Copyright 2017 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from time import sleep
from google.cloud import pubsub
import random
import argparse


class EventProducer(object):

    def __init__(self, project_id, topic_name):
        self._client = pubsub.PublisherClient()
        self._topic_path = self._client.topic_path(project_id, topic_name)

    def publish(self, message):
        data = message.encode("utf-8")
        self._client.publish(self._topic_path, data)


def get_random_record():
    return ",".join([
        random.choice(["CA", "DC", "WA", "OH"]),    # state_abbreviation
        random.choice(["F", "M"]),  # gender
        str(random.randint(1920, 2010)),    # year
        "*" + random.choice(["Rolanda", "Georgann", "Carlotta", "Joe", "Gretchen", "Marco"]),   # name,
        str(random.randint(100, 1000)),  # count of babies
        "28/02/2019"    # dataset created
    ])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--project', dest='project', required=True)
    parser.add_argument('--topic', dest='topic', required=True)

    params = parser.parse_args()

    producer = EventProducer(params.project, params.topic)

    while True:
        producer.publish(get_random_record())
        sleep(5)
