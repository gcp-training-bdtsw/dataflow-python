# Copyright 2017 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import absolute_import
import argparse
import logging
import re
import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions


class DataIngestion:

    def parse_method(self, string_input):
        """ >>>
            state_abbreviation,gender,year,name,count_of_babies,dataset_created_date
                example string_input: KS,F,1923,Dorothy,654,11/28/2016

        Returns:
              {'state': 'KS',
               'gender': 'F',
               'year': '1923', <- Keep the format of the "date" as a simple integer, with no transformations.
               'name': 'Dorothy',
               'number': '654',
               'created_date': '11/28/2016'
               }
         """
        # Strip out return characters and quote characters.
        values = re.split(",", string_input)
        row = dict(zip(('state', 'gender', 'year', 'name', 'number', 'created_date'), values))

        return row


def run(argv=None):
    """The main function which creates the pipeline and runs it."""
    parser = argparse.ArgumentParser()

    parser.add_argument('--input', dest='input', required=False,
        help='Input file to read.  This can be a local file or a file in a Google Storage Bucket.',
        default='gs://python-dataflow-example/data_files/head_usa_names.csv')

    parser.add_argument('--output', dest='output', required=False,
        help='Output BQ table to write results to.',
        default='lake.usa_names')

    known_args, pipeline_args = parser.parse_known_args(argv)
    
    data_ingestion = DataIngestion()

    p = beam.Pipeline(options=PipelineOptions(pipeline_args))

    lines = p | 'Read from GCS File' >> beam.io.ReadFromText(known_args.input, skip_header_lines=1)

    # lines = p | 'Read from PubSub Queue' >> beam.io.ReadStringsFromPubSub(topic=known_args.input)

    (lines
     | 'String To BigQuery Row' >> beam.Map(lambda s: data_ingestion.parse_method(s))
     | 'Write to BigQuery' >> beam.io.WriteToBigQuery(
            known_args.output,  # Target table name
            schema='state:STRING,gender:STRING,year:STRING,name:STRING,number:STRING,created_date:STRING',
            create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
            write_disposition=beam.io.BigQueryDisposition.WRITE_EMPTY)
     )

    p.run()


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    run()
